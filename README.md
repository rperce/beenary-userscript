# Beenary Display Userscript
## And also goal collapsing

All credit for collapsing (though I impelmented it cleanroom) to zzq on the Beeminder
forums.

My primary browser, [qutebrowser](https://qutebrowser.org/), can't load extensions, but
_can_ load greasemonkey userscripts, so I cleanroom-ported the goal collapsing to a
userscript and added a cute display for beenary goals.
