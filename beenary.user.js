// ==UserScript==
// @name Beenary
// @namespace https://rperce.net/
// @description Beenary goal display
// @include https://www.beeminder.com/*
// @version 3.1.0
// ==/UserScript==

/*************
 * Utilities *
 *************/

const delta_fmt = (timestamp) => {
  const delta = timestamp - Date.now();
  for ([ms, sigil] of [
    [1000 * 60 * 60 * 24 * 7, "w"],
    [1000 * 60 * 60 * 24, "d"],
    [1000 * 60 * 60, "h"],
    [1000 * 60, "m"],
  ]) {
    const div = Math.floor(delta / ms);
    if (div > 0) {
      return div + sigil;
    }
  }
  return "??";
};

const load = (key) => window.sessionStorage.getItem(`beenary.user.js::${key}`);
const loadArray = (key) => {
  const val = load(key);
  return val ? val.split(",") : [];
};
const store = (key, newValue) => {
  key = `beenary.user.js::${key}`;
  const oldValue = load(key);
  const storageArea = window.sessionStorage;
  storageArea.setItem(key, newValue);
  window.dispatchEvent(
    new StorageEvent("storage", { key, oldValue, newValue, storageArea })
  );
};

const partition = (array, predicate) => {
  const ifTrue = [];
  const ifFalse = [];
  array?.forEach((x) => (predicate(x) ? ifTrue : ifFalse).push(x));
  return [ifTrue, ifFalse];
};

const allGoals = () =>
  Array.from(document.querySelectorAll("#goals-active .goal"));
const isBeenary = (goalNode) =>
  goalNode
    .querySelector(".descriptors .title")
    ?.textContent.toLowerCase()
    .includes("beenary");
const hasTodayta = (goalNode) =>
  window.getComputedStyle(goalNode.querySelector(".todayta")).display !==
  "none";
const getSlug = (goalNode) => goalNode.getAttribute("data-slug");

/********************
 * Node definitions *
 ********************/

const CuteDisplay = (slug, color, doom, todayta) => {
  const doomNode = document.createElement("div");
  doomNode.innerText = doom;
  doomNode.style.fontSize = "150%";

  const lineNode = document.createElement("div");
  lineNode.style.borderBottom = `3px solid ${color || "black"}`;
  lineNode.style.width = "100%";

  const slugNode = document.createElement("div");
  slugNode.innerText = slug;
  slugNode.style.fontSize = "85%";

  const node = document.createElement("button");
  node.style.display = "flex";
  node.style.flexDirection = "column";
  node.style.minWidth = "4em";
  node.style.marginRight = "0.7rem";
  node.style.marginBottom = "0.7rem";
  node.style.alignItems = "center";
  node.style.background = "none";
  node.style.border = "none";
  node.style.padding = "0.3rem";
  if (!todayta) {
    node.style.cursor = "pointer";
    node.addEventListener("mouseover", () => {
      node.style["background"] = "#eee";
      node.style["box-shadow"] = "4px 4px 3px 0px #667284";
    });
    node.addEventListener("mouseout", () => {
      node.style["background"] = "none";
      node.style["box-shadow"] = null;
    });
    node.setAttribute("title", "Click to +1");
    node.addEventListener("click", () => {
      doomNode.innerText = "";
      doomNode.classList.add("lds-dual-ring"); // loaders.io
      const form = document.querySelector(`.goal[data-slug="${slug}"] form`);
      form.style.display = "block";
      form.querySelector('input[name="datapoint-value"]').value = 1;
      form.querySelector(
        'input[name="datapoint-comment"]'
      ).value = `Submitted with beenary.user.js at ${new Date().toTimeString()}`;
      form.querySelector('input[type="submit"]').click();
      form.style.display = null;
    });
  }

  node.append(doomNode, lineNode, slugNode);

  return node;
};

const Row = (id, ...children) => {
  const row = document.createElement("div");
  row.id = id;
  row.style.display = "flex";
  row.style.marginTop = "1rem";
  row.style.marginBottom = "1rem";
  row.style.overflowX = "auto";

  row.append(...children);

  return row;
};

const CollapsedGoal = (slug, baremin, delta, todayta) => {
  const textNode = document.createTextNode(`${slug} (${baremin} in ${delta})`);

  const fillerNode = document.createElement("div");
  fillerNode.style.flexGrow = "1";

  const todaytaNode = document.createElement("div");
  todaytaNode.className = "octicon octicon-check todayta";
  todaytaNode.style.display = todayta ? "block" : "none";

  const node = document.createElement("div");
  node.style.padding = "4px 10px 4px 8px";
  node.style.display = "flex";
  node.style.backgroundColor = "#eee";
  node.style.marginTop = "0.5rem";
  node.className = "collapsed-goal";
  node.style.cursor = "pointer";
  node.onclick = () => {
    ["beenary-todayta", "user-collapsed"].forEach((key) => {
      const set = new Set(loadArray(key));
      if (set.has(slug)) {
        set.delete(slug);
        store(key, Array.from(set));
      }
    });
  };

  node.append(textNode, fillerNode, todaytaNode);

  return node;
};

const LinkyButton = () => {
  const button = document.createElement("button");
  button.style.background = "none";
  button.style.border = "none";
  button.style.padding = "0";
  button.style.textDecoration = "underline";
  button.style.cursor = "pointer";
  button.style.marginLeft = "0.5rem";

  return button;
};

const CollapseButton = (slug) => {
  const button = LinkyButton();
  button.classList.add("collapse-button");
  button.innerText = "collapse";
  button.addEventListener("click", () => {
    const form = document.querySelector(`.goal[data-slug="${slug}"] form`);
    if (form?.style) form.style.display = null;
    store("user-collapsed", loadArray("user-collapsed").concat([slug]));
  });

  return button;
};

const UncollapseButton = (slug) => {
  const button = LinkyButton();
  button.style.display = "none";
  button.classList.add("uncollapse-button");
  button.innerText = "restore";
  button.addEventListener("click", () => {
    store("user-collapsed", loadArray("user-collapsed").concat([slug]));
    ["beenary-todayta", "user-collapsed"].forEach((key) => {
      const set = new Set(loadArray(key));
      if (set.has(slug)) {
        set.delete(slug);
        store(key, Array.from(set));
      }
    });

    const form = document.querySelector(`.goal[data-slug="${slug}"] form`);
    const toggleSelector = `.expanded-toggle[data-slug="${slug}"] .mega-octicon`;
    const chevron = document.querySelector(toggleSelector);
    const expanded = chevron.classList.contains("octicon-chevron-down");
    form.style.display = expanded ? "block" : null;
  });

  return button;
};

/*******************
 * Beenary display *
 *******************/

generateBeenaryDisplay = () => {
  const todaytaSlugs = new Set(loadArray("beenary-todayta"));
  const [todayta, waiting] = partition(allGoals().filter(isBeenary), (g) =>
    todaytaSlugs.has(getSlug(g))
  );

  const existing = document.querySelector("#beenary-display");
  existing?.parentNode?.removeChild(existing);

  const collapsed = new Set(loadArray("user-collapsed"));
  const [waitingCollapsed, waitingShown] = partition(waiting, (g) =>
    collapsed.has(getSlug(g))
  );
  const content = document.querySelector(".content.dashboard");
  content.prepend(
    Row(
      "beenary-display",
      ...waitingShown.map((node) => {
        const slug = getSlug(node);
        const doom = delta_fmt(
          parseInt(node.getAttribute("data-losedate")) * 1000
        );
        const color = window.getComputedStyle(
          node.querySelector(".expanded-toggle")
        ).backgroundColor;
        return CuteDisplay(slug, color, doom, false);
      }),
      ...waitingCollapsed.map((node) => {
        const slug = getSlug(node);
        const doom = delta_fmt(
          parseInt(node.getAttribute("data-losedate")) * 1000
        );
        const color = "gray";
        const display = CuteDisplay(slug, color, doom, false);
        display.style.color = "gray";
        return display;
      }),
      ...todayta.map((node) => {
        const slug = getSlug(node);
        const color = "gray";
        const doom = "✓";
        return CuteDisplay(slug, color, doom, true);
      })
    )
  );
};

/**************
 * Collapsing *
 **************/

const collapseGoals = () => {
  document.querySelectorAll(".collapsed-goal")?.forEach((node) => {
    node.classList.remove("collapsed-goal");
    node.querySelector(".collapse-button").style.display = null;
    node.querySelector(".uncollapse-button").style.display = "none";
  });

  const collapsedSlugs = new Set([
    ...loadArray("beenary-todayta"),
    ...loadArray("user-collapsed"),
  ]);

  allGoals().forEach((node) => {
    const slug = getSlug(node);
    if (node.classList.contains("red")) {
      node.querySelector(".collapse-button").style.display = "none";
    }

    if (collapsedSlugs.has(slug)) {
      node.classList.add("collapsed-goal");
      //node.querySelector(".pledge").previousSibling.remove();
      const summary = node.querySelector(".summary");
      const or_pay = node.querySelector(".pledge").previousSibling;
      if (or_pay.nodeType === Node.TEXT_NODE) summary.removeChild(or_pay);

      node.querySelector(".collapse-button").style.display = "none";
      node.querySelector(".uncollapse-button").style.display = null;
      const dataEntryForm = node.querySelector("form");
      if (dataEntryForm) dataEntryForm.style.display = null;

      const refreshWrapper = node.querySelector(".refresh-wrapper");
      if (refreshWrapper) refreshWrapper.style.display = null;
    }
  });
};

const addCollapseButtons = (goals) => {
  goals.forEach((goalNode) => {
    const slug = getSlug(goalNode);
    goalNode.querySelector(".summary").append(CollapseButton(slug));
    goalNode.querySelector(".summary").append(UncollapseButton(slug));
  });
};

const squish = (string) =>
  string.replace(/[\r\n\t]+/g, " ").replace(/' {2,}/g, " ");
const addCss = () => {
  const cssText = squish(`
    #goals-active {
      display: flex;
      flex-direction: column;
    }

    .dashboard.content .goal.collapsed-goal {
      padding-bottom: 0.5rem;
      padding-right: 0.5rem;
      order: 99
    }

    .dashboard.content .goal.collapsed-goal .expanded-toggle,
    .dashboard.content .goal.collapsed-goal .refresh-wrapper,
    .dashboard.content .goal.collapsed-goal .infinibee-parent,
    .dashboard.content .goal.collapsed-goal .dashboard-data,
    .dashboard.content .goal.collapsed-goal .last-datapoint,
    .dashboard.content .goal.collapsed-goal .title,
    .dashboard.content .goal.collapsed-goal .pledge,
    .dashboard.content .goal.collapsed-goal form {
      display: none
    }

    .dashboard.content .goal.collapsed-goal a.slug,
    .dashboard.content .goal.collapsed-goal a.slug.solo,
    .dashboard.content .goal.collapsed-goal .summary,
    .dashboard.content .goal.collapsed-goal .baremin,
    .dashboard.content .goal.collapsed-goal .doom,
    .dashboard.content .goal.collapsed-goal .descriptors,
    .dashboard.content .goal.collapsed-goal .octicon {
      height: min-content;
      line-height: 24px;
    }

    .collapsed-goal .baremin::before {
      content: '('
    }

    .collapsed-goal .doom::after {
      content: ')'
    }
    .lds-dual-ring {
      display: inline-block;
      width: 2rem;
      height: 2rem;
    }
    .lds-dual-ring:after {
      content: " ";
      display: block;
      width: 1.6rem;
      height: 1.6rem;
      margin: 0.2rem;
      border-radius: 50%;
      border: 0.1rem solid black;
      border-color: black transparent black transparent;
      animation: lds-dual-ring 1.2s linear infinite;
    }
    @keyframes lds-dual-ring {
      0% {
        transform: rotate(0deg);
      }
      100% {
        transform: rotate(360deg);
      }
    }
  `);

  const css = document.createElement("style");
  css.textContent = cssText;
  document.body.appendChild(css);
};

/********
 * main *
 ********/

window.addEventListener("keypress", (evt) => {
  if (document.activeElement instanceof HTMLInputElement) return;
  if (evt.key === "o") {
    store("beenary-todayta", "");
    store("user-collapsed", "");
  }
});

window.addEventListener("load", () => {
  addCss();

  const goals = allGoals();
  const [todayta] = partition(goals.filter(isBeenary), hasTodayta);

  addCollapseButtons(goals);

  window.addEventListener("storage", (evt) => {
    if (
      evt.storageArea !== window.sessionStorage ||
      !evt.key.startsWith("beenary.user.js::")
    )
      return;

    generateBeenaryDisplay();
    collapseGoals();
  });
  store("beenary-todayta", todayta.map(getSlug));

  new MutationObserver((mutationList) => {
    const isStyleMut = (mut) => mut.attributeName === "style";

    // mut.oldValue is the string value of the attribute in question, so something like 'min-height: 7px'
    const wasHeightMax = (mut) => !!mut.oldValue?.match(/min-height:\s*75px/);

    if (mutationList.some((mut) => isStyleMut(mut) && wasHeightMax(mut))) {
      const [todayta] = partition(allGoals().filter(isBeenary), hasTodayta);
      store("beenary-todayta", todayta.map(getSlug));
    }
  }).observe(document.querySelector("#flash-js"), {
    attributes: true,
    attributeOldValue: true,
  });
});

console.log("beenary");
